package coordinates;

public class PointList {
    double[] pointList = new double[0];

    public double[] add(Point point) {
        double[] pointList = new double[this.pointList.length + 2];
        if (this.pointList.length == 0) {
            for (int i = 0; i < pointList.length; i++) {
                pointList[i] = point.getX();
                i++;
                pointList[i] = point.getY();
            }
        } else {
            for (int i = 0; i < this.pointList.length; i++) {
                pointList[i] = this.pointList[i];

            }

            for (int i = this.pointList.length; i < pointList.length; i++) {
                pointList[i] = point.getX();
                pointList[++i] = point.getY();
            }

        }
        this.pointList = pointList;
        return this.pointList;

    }


    public int size() {
        return pointList.length;
    }

    public double getIndex(int index) {
        return pointList[index];
    }

}
